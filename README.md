# Log
Le (weB)log de notre site. 
### Objectif
Nous cherchons à réaliser une interface Web-Log afin de faire du suivi de
projets. Cet outil doit servir à un pratique quotidienne, donc doit-être facile à alimenter. Son objectif est
de faire trace des réfléxions et des étapes d'un projet en cours.
Les média doivent être multiple: texte plus ou moins long, images, videos, son. 

## Liste des moyens technique

### exemple cms

CMS Made simple (https://framalibre.org/content/cms-made-simple)
99 ko  (http://99ko.org/)

### Nos différents blogs

Alec & Étienne -> [interstices.io](http://interstices.io)

Étienne -> [Pour un design graphique libre](http://www.etienneozeray.fr/libre-blog/)

Antoine -> Archi-trace (r.i.p)
![archi-trace](ressources/archi-trace.png)

Romain -> [desinformations](http://desinformations-links.tumblr.com/)

Romain -> [Ivro](http://ivro.fr/)

Alec -> [blog list](http://alexandre.atelier-bek.be/blog/www/)


### Autres Blogs

la Villa Hermosa -> [blog](http://blog.lavillahermosa.com/)



