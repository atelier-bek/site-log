function LoadPost(){
  $('li.post_btn').click(function(){
    var src = $(this).attr('data-src');
    $('.content').load('templates/post.php?src='+src);
  })
}

$(document).ready(function(){
  LoadPost();
})
