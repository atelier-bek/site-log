<?php

function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

function makeMeta($text){
 global $post;
 $post = [];
 $metaInfo = get_string_between($text, '---', '---');

 $ligne = preg_split("/[\n]+/", $metaInfo);
  foreach( $ligne as $row => $value ) {
    $explo = explode(':', $value);
    $post[$explo[0]] = $explo[1];
  }
}

function ParseShow($dir){
  global $post_content;
 $parsedown = new Parsedown();
 $text = file_get_contents($dir);

 $post_content = $parsedown->text($text);
 makeMeta($text);
}

function ScanDirectory($Directory){

  $MyDirectory = opendir($Directory) or die('Erreur');
  while($Entry = @readdir($MyDirectory)) {
      if (($Entry!='.')&&($Entry!='..')&&($Entry!='.git')) {
        $textMeta = file_get_contents($Directory.$Entry);
        $post = [];
        $metaInfo = get_string_between($textMeta, '---', '---');

        $ligne = preg_split("/[\n]+/", $metaInfo);
          foreach( $ligne as $row => $value ) {
            $explo = explode(':', $value);
            $post[$explo[0]] = $explo[1];
          }
        echo '<li title="'.$Entry.'" class="post_btn" data-src="'.$Directory.$Entry.'" ><span>'.$post['date'].'</span>'.$post['title'].'<span>'.$post['author'].'</span></li>';
    }
  }
  closedir($MyDirectory);
}

?>
