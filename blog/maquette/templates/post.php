<?php
  require_once '../functions/Parsedown.php';
  require_once '../functions/functions.php';
  $src_post = $_GET['src'];
  ParseShow('../'.$src_post);
?>
<header>
  <h1><?php echo $post['title']; ?></h1> -
  <h3><?php echo $post['date']; ?></h3> -
  <h2><?php echo $post['author']; ?></h2>
</header>

<div class="text_cont" >
  <?php echo $post_content;?>
</div>
